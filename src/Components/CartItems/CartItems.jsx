
import Modal from "../Modal/Modal";
import { useDispatch, useSelector } from "react-redux";
import { removeFromCart, modalClose, modalOpen } from "../../reducers";


import './CartItems.scss'

const CartItems = () => {


    const dispatch = useDispatch()
    const modal = useSelector(state => state.modal.isModal)
    const cart = useSelector(state => state.cart.cartToLocal)


    return (

        <>

            {!cart.length && <div className='cart-empty'>
                <strong> Cart is empty</strong>
            </div>}
            <div className="products-wrapper">
                
                {cart.map((el) => <div key={el.id}>

                    <div className="card-wrapper">
                        <div className="remove-card" onClick={() => { 
                            dispatch(modalOpen())
                             }}>X</div>

                        <div className="img-wrapper">
                            <img className="image" src={el.img} alt={el.name} />
                        </div>
                        <div className="flex-wrapp">
                            <h1 className="device-name">{el.name}</h1>
                       
                        </div>
                        <p className="art">Art: {el.art}</p>
                        <p className="color">Color: {el.color}</p>
                        <div className="price-wrapp">

                            <p className="price">$ {el.price}</p>

                    {modal && <Modal text='Do you want to continue?'
                        onCancel={() => { dispatch(modalClose()) }}
                        onConfirm={() => {
                    
                            dispatch(removeFromCart(el))
                            dispatch(modalClose())
                    
                        }
                        }
                    
                    />}

                        </div>

                    </div>

                </div>

)}

            </div>
        </>


    )

};

export default CartItems
