import { createSlice } from '@reduxjs/toolkit'

 const modalSlice = createSlice({
    name: 'modal',
    initialState: {
        isModal: false,
        
    },
    reducers: {
   
        modalOpen(state) {
            state.isModal = true
        },
        modalClose(state) {
            state.isModal = false
        },
 
    },
})

export const {modalOpen,modalClose} = modalSlice.actions
export default modalSlice.reducer